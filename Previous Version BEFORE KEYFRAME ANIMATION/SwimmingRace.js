/*###################################################################################################################################################################
#       FILENAME - SwimmingRace.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                         #
#                                                                                                                                                                   # 
#       THE SWIMMING RACE USING Javascript and Three.js                                                                                                             #
#       WRITTEN BY CAMERON WATT s3589163                                                                                                                            #
#       COMPLETED 19 MAY 2016                                                                                                                                       #
#                                                                                                                                                                   #
#       THIS FILE CALLS EACH OF THE FUNCTIONS FROM THE OTHER FILES                                                                                                  #
#       ### IMPORTANT NOTE ### THE SWIMMERS MOVE ALONG THE Z AXIS, EVEN IF THE CAMERA ANGLE IS CHANGED TO ALLOW THE BEST VIEW OF THE SWIMMERS                       #
#                                                                                                                                                                   # 
#       // ################################################################################################################                                         #
#       VARIABLES CONTAINED WITHIN THESE ARE INTENDED TO ALLOW USERS TO CHANGE THE MODEL, OR ASPECT OF IT EASILY                                                    #
#       INCLUDING   - THE SIZE / SHAPE OF THE POOL                                                                                                                  #
#                   - THE WATER HEIGHT                                                                                                                              #
#                   - THE SIZES OF THE SWIMMERS                                                                                                                     #
#                   - THE NUMBER OF SWIMMERS AND LANES IN THE POOL                                                                                                  #
#                   - OVERALL ANIMATION SPEED                                                                                                                       #
#       EACH OF THESE CHANGES SHOULD SCALE THROUGH TO THE REST OF THE PROGRAM, ALLOWING SMOOTH CHANGES TO BOTH THE VISUAL AND PROGRAM ELEMENTS EG SWIMMER MOVEMENT  #
#       // ################################################################################################################                                         #
#                                                                                                                                                                   #
###################################################################################################################################################################*/

// ### WORLD VARIABLES ###
var geometry;   // GENERIC geometry VARIABLE;

// TIME VARIABLES
var dt = 0;     // USED TO STORE THE TIME DELTA'S
var clock = new THREE.Clock();
clock.start();

var raceInProgress = false;    // USED TO START AND STOP THE RACE

// OVERALL SPEED OF THE ANIMATIONS
// ###################################################################
var animationSpeed = 4; 
// ###################################################################


// ### POOL SHAPE VARIABLES ###
// #########################################################################################################################
// PLEASE NOTE THAT EACH SWIMMER IS APROXIMATELY (swimmerX * 6) UNITS WIDE (IN X)
// IF THE VALUES OF THE SWIMMER OR THE POOL ARE CHANGED, THIS MUST BE CONSIDERED, OR OBJECT WILL START TO OVERLAP

// CHANGE THESE VARIABLES TO SCALE THE SIZE OF THE POOL AND ALL ELEMENTS WITHIN IT
var poolX = 10;     // HALF OF THE WIDTH OF THE POOL  (DUE TO -X TO +X)
var poolY = 3;      // DEPTH OF THE POOL (THIS IS THE TOTAL DEPTH, AS IT ONLY EXTENDS FROM 0 TO +Y)  
var poolZ = 25;     // HALF OF THE LENGTH OF THE POOL  (DUE TO -Z TO +Z)

var waterHeight = (poolY * 0.85);   // HEIGHT OF THE WATER WITHIN THE POOL ( SWIMMERS ARE ALSO CREATED AT THIS HEIGHT )
// #########################################################################################################################

var pool =  undefined;

// LOAD THE TEXTURE READY TO BE ADDED TO THE WALLS OF THE POOL
var poolTextureLoader = new THREE.TextureLoader();
var poolTexture = poolTextureLoader.load("./textures/tiles.jpg");

var poolMaterial = new THREE.MeshBasicMaterial(
    {
        color: 0x0090ff,
        //opacity: 0.25,
        map: poolTexture
    } );

var waterMaterial = new THREE.MeshBasicMaterial(
    {
        color: 0x0000ff,  
        transparent: true,
        opacity: 0.75
    } );

// ### LANE VARIABLES ###
// ###############################################################################
var numberOfLanes = 8;          // DEFINES THE TOTAL NUMBER OF LANES IN THE POOL
var ropeColorHex = 0xfff000;    // SETS THE COLOR OF THE POOL DIVIDERS
// ###############################################################################

var widthOfEachLane = (2 * poolX / numberOfLanes);   // TOTAL WIDTH OF THE POOL DIVIDED BY THE NUMBER OF LANES FROM ABOVE

// ROPE VARIABLES
var rope;   // EMPTY VARIABLE USED TO CONTAIN THE OBJECT
var ropeMaterial = new THREE.MeshBasicMaterial( { color: ropeColorHex } );


// ### SWIMMER VARIABLES ###
// #####################################################################################################################################################################
// OCTAHEDRON AND DECAHEDRON SIZES      THESE CAN BE SET TO ALMOST ANY SIZE AND THE PROGRAM WILL CALCULATE THE NEW DIMENSIONS OF EACH SWIMMER
// DUE TO THE SIZE OF THE POOL, THESE SIZES MUST BE QUITE SMALL.  SIZES HERE MUST BE LESS THAN 0.375, 0.225, 0.225 TO ALLOW EACH SWIMMER TO FIT INTO LANES OF SIZE 2 WIDE
var swimmerX = 0.25;
var swimmerY = 0.15;
var swimmerZ = 0.15;

// AXIS BARS USED ON THE SWIMMERS
var axesLength = 0.25;   // USED TO DETERMINE THE SIZE OF THE COLOURED AXIS BARS

// STARTING MATERIALS FOR THE SWIMMER MODEL.  THE COLOUR OF THE MODEL CAN BE CHANGED HERE, AND WILL FILTER THROUGH TO EACH OF THE BODY PARTS (EXCEPT THE EYES)
var swimmerColorHex = 0xff9966; 
// #####################################################################################################################################################################

var swimmerMaterial = new THREE.MeshBasicMaterial( { color: swimmerColorHex } );

// SET THE COLOR FOR THE EYES
var eyeMaterial = new THREE.MeshBasicMaterial( {color: 0x0000cc } );

swimsuitList = new Array;   // TO CONTAIN EACH OF THE TEXTURED SWIMSUITS

var swimsuitTextureLoader = new THREE.TextureLoader();

// LOAD CLARK's SWIMSUIT TEXTURES
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style0.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style1.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style2.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style3.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style4.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style5.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style6.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style7.png" ) );
swimsuitList.push(swimsuitTextureLoader.load( "./textures/style8.png" ) );     // NOT NEEDED, AS THERE ARE ONLY 8 SWIMMERS 

var BodyPartList = new Array(); // STORE THE VALUES FOR EACH OF THE PARTS CREATED

var swimmerList = new Array();  // STORE THE SWIMMERS CREATED - TO ALLOW INDIVIDUAL MOVEMENT

var swimmerSpeed = new Array(); // STORE THE CURRENT SPEEDS FOR EACH OF THE SWIMMERS

var swimmerDirection = new Array();    // STORES THE CURRENT DIRECTION FOR EACH SWIMMER - STORES 0 WHEN THE SWIMMER IS STOPPED

var finalPositions = new Array(); // STORES THE NUMBER OF THE SWIMMERS IN THE ORDER THEY CROSSED THE FINISH LINE
var currentPosition = 1;  // STORES THE CURRENT POSITION TO BE AWARDED TO THE NEXT SWIMMER TO TOUCH THE FINISHING LINE

var direction = -1 ; // USED TO CHANGE THE DIRECTION OF THE SWIMMERS  NEGATIVE TO GO "INTO" THE SCREEN, AND POSITIVE TO RETURN


// ### SCENE VARIABLES ###
var scene = new THREE.Scene();

// SET THE CAMERA TO SHOW THE SWIMMERS
var camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000); 

// ###############################################################################
// THIS VARIABLE IS USED TO CHANGE THE CAMERA ANGLE, BETWEEN THE Y AND Z AXIS, SIMPLY SET THIS VARIABLE TO BE 'y' OR 'z'
var poolAxis = "y";
// ###############################################################################
changeCameraAngle() // SET THE CAMERA TO ITS DESIGNATED POSITION THIS HAS TO BE AFTER THE RENDERER IS DEFINED

// TRACKBALL CONTROLS
var controls = new THREE.TrackballControls(camera);     
controls.addEventListener('change', render);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0x404040, 1);



document.body.appendChild(renderer.domElement); 




window.onload = function()
{
    // ### CREATE POOL AND ROPE DIVIDERS ### 

    // CREATE THE POOL AND ADD THEM TO THE SCENE
    pool = new createPool( geometry, poolX, poolY, poolZ, poolMaterial, poolTexture );
    scene.add(pool); 

    water = new createWater( geometry, poolX, waterHeight, poolZ, waterMaterial )
    scene.add(water);

    // CREATE THE LANE DIVIDERS AND ADD THEM TO THE SCENE
    for (var i = 1; i < numberOfLanes; i++)
    {
        rope = new createRopeDivider(poolX, waterHeight, poolZ, geometry, ropeMaterial);
        
        rope.position.x += ( i * widthOfEachLane );    // MOVE TO THE EDGE OF THE NEXT LANE, THEN ADD THE NEXT ROPE DIVIDER
        
        scene.add(rope)
    }

    // ### CREATE SWIMMERS ### 
    for (var i = 0; i < numberOfLanes; i++)
    {

        swimsuitMaterial = new THREE.MeshBasicMaterial(
        {
            color: 0xffffff,
            map: swimsuitList[i]
        } );

        swimmer = new createSwimmer(geometry, swimmerX, swimmerY, swimmerZ, swimmerMaterial, eyeMaterial, swimsuitMaterial );     // DRAW THE SWIMMER
        swimmerList.push(swimmer);  // ADD THE NEWLY CREATED SWIMMER TO THE swimmerList ARRAY

        // ROTATE THE SWIMMER INTO POSITION
        swimmer.rotation.x = ( 3.14 / 2 );      // ROTATE SWIMMER 90 DEGREES TO BE ALONG THE Z AXIS
        swimmer.rotation.z = 3.14 ;             // ROTATE SWIMMER 180 DEGREES TO FACE CORRECT DIRECTION

        // MOVE SWIMMER TO THE STARTING POSITION
        swimmer.position.z += ( poolZ - (swimmerX * 7) );   // MOVE SWIMMER TO THE ( STARTING LINE - swimmers ) APROXIMATE LENGTH
        swimmer.position.y = ( waterHeight );   // MOVE SWIMMER UP TO THE HEIGHT OF THE WATER
        swimmer.position.x -= poolX;    // START FROM THE LEFT HAND EDGE OF THE POOL
        swimmer.position.x += ( 0.5 * widthOfEachLane )     // MOVE SWIMMER TO THE MIDDLE OF THE FIRST LANE
        swimmer.position.x += ( i * widthOfEachLane )     // MOVE NEXT SWIMMER TO THE MIDDLE OF NEXT LANE
        
        swimmerDirection[ i ] = -1 ; // SET THE SWIMMER TO SWIM RIGHT TO LEFT (TOWARDS -VE Z)

        scene.add(swimmer);
    }
    render()
    animate();

}   // END OF window.onload = function()


// ===== END OF MAIN FUNCTION, START OF OTHER FUNCTIONS ================================================================================================================


function changeCameraAngle()
{
    if ( poolAxis == "y" )
    {
        // USE THESE COORDINATES TO SHOW THE POOL ALONG THE Z AXIS
        poolAxis = "z";
        camera.position.x = 0;
        camera.position.y = ( poolY * 7 );        
        camera.position.z = ( poolZ * 1.55 );
    }
    else if ( poolAxis == "z" )
    {
        // USE THESE COORDINATES TO SHOW THE POOL ALONG THE Y AXIS
        poolAxis = "y";
        camera.position.x = ( -poolX * 1.5 );
        camera.position.y = ( poolY * 7 );
        camera.position.z = 0;
    }
}



function animate()
{

    controls.update();

    dt = clock.getDelta();

    // MOVE THE SWIMMERS WHILE THE RACE IS IN PROGRESS
    if ( raceInProgress == true )
    {
        swimmerMovement();
    }

    render();
    requestAnimationFrame( animate );
}



function render()
{
    renderer.render( scene, camera );
}
