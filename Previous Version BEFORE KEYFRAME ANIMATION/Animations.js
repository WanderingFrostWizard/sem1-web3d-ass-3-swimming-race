/*###################################################################################################################################################################
#       FILENAME - Animations.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                           #
#                                                                                                                                                                   # 
#       THE SWIMMING RACE USING Javascript and Three.js                                                                                                             #
#       WRITTEN BY CAMERON WATT s3589163                                                                                                                            #
#       COMPLETED 19 MAY 2016                                                                                                                                       #
#                                                                                                                                                                   #
#       THIS FILE CREATES THE ANIMATIONS USED WITHIN THE SwimmingRace.js FILE                                                                                       #
#       MOST OF THIS INTERPOLATE CODE IS FROM THE TUTORIAL EXERCISES AND CLASSES - WHICH HAS BEEN RE-WORKED FROM GEOFF AND CLARKS EXAMPLES                          #
#       I HAVE MANAGED TO UNDERSTAND ENOUGH ABOUT THE ANIMATIONS TO GET THEM WORKING, BUT I NEED TO UNDERSTAND THEM BETTER TO BE ABLE TO WRITE THIS CODE MYSELF     #
#                                                                                                                                                                   #
###################################################################################################################################################################*/

var v = 0;      // VALUE OF THE INTERVAL VALUE  - CURRENTLY UNKNOWN

var keys   = [ 0,  5, 10, 15 ];
var values = [ 0, 10, 20, 30 ];


function interpolate( keys, values, k )
{
    interval = findInterval( keys, k )
    k1 = keys[ interval - 1 ];
    k2 = keys[ interval ];

    v1 = values[ interval - 1] ;
    v2 = values[ interval ];

    v = lerp( k1, v1, k2, v2, k );

    return v;
}

function lerp( k1, v1, k2, v2, k )        // INTERPOLATER FUNCTION
{ 
    v = ( k - k1 ) / ( k2 - k1 ) * ( v2 - v1 ) + v1;
    return v;
}

function findInterval( keys, k )
{
    for ( var i = 0; i < keys.length; i += 1 ) 
    {
        if ( keys[i] > k )
        {
            if ( i == 0 ) // INVALID, AS THE FIRST ELEMENT OF THE KEYS ARRAY IS GREATER THAN THE GIVEN KEY VALUE
            {
                console.log( "The value of 'k' is LESS than ALL the values within the keys Array." );
            }
            else
            {
                return i;   // RETURN THE INDEX OF THE FIRST ELEMENT WITHIN KEYS THAT IS GREATER THAN k
                break;  // END THE FOR LOOP
            }
        }   
        else if ( i == ( keys.length - 1 ) )
        {
            console.log( "The value of 'k' is GREATER than ALL the values within the keys Array." )
        }
    }
}

function swimmerMovement()
{
    for (var i = 0; i < numberOfLanes; i++)
    {
        swimmerSpeed[ i ] = ( Math.random() * dt * animationSpeed );
        swimmerList[ i ].position.z += swimmerDirection[ i ] * interpolate( keys, values, swimmerSpeed[ i ] );

        // IF THE SWIMMERS HAND HAS REACHED THE END OF THE POOL, ROTATE SWIMMER TO FACE THE START OF THE RACE
        if ( swimmerList[i].position.z < ( -poolZ + swimmerX * 6 ) ) 
        {
            swimmerList[i].rotation.z = 0; // CHANGE DIRECTION OF SWIMMER TO POSITIVE Z
            swimmerDirection[ i ] = 1 ;    // // SET THE SWIMMER TO SWIM LEFT TO RIGHT (TOWARDS +VE Z)
        }
        else if ( (swimmerDirection[ i ] != 0 ) && ( swimmerList[i].position.z > ( poolZ - swimmerX * 6 ) ) )
        {
            swimmerDirection[ i ] = 0;  // STOP THE CURRENT SWIMMER

            // RECORD THE CURRENT SWIMMERS FINAL POSITION IN THE RACE
            finalPositions[ currentPosition - 1 ] = i;    
            currentPosition++; 
        }
    }
}

// SETS ALL SWIMMERS DIRECTIONS TO GO TO COMMENCE THEIR MOVEMENT, WITHOUT AFFECTING THEIR POSITIONS IN THE RACE    
function startRace()
{
    raceInProgress = true;
}

// SETS ALL SWIMMERS DIRECTIONS TO ZERO TO HALT THEIR MOVEMENT, WITHOUT AFFECTING THEIR POSITIONS IN THE RACE
function stopRace()
{
    raceInProgress = false;
}

function showScoreboard()
{
    // POSITIONS ARE STORED AS THE ( Position - 1 ) WITHIN THE finalPositions ARRAY
    for (var i = 0; i < numberOfLanes; i++)
    {
        console.log("POSITION: " + ( i + 1 ) + " SWIMMER: " + ( finalPositions[ i ] + 1 ) );
    }
}