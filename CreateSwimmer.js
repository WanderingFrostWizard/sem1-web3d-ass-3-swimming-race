/*###################################################################################################################################################################
#       FILENAME - CreateSwimmer.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                        #
#                                                                                                                                                                   # 
#       BOB AND THE SWIMMING RACE IN Javascript and Three.js                                                                                                        #
#       WRITTEN BY CAMERON WATT S3589163                                                                                                                            #
#       COMPLETED 19 MAY 2016                                                                                                                                          #
#                                                                                                                                                                   #
#       THIS FILE CREATES THE SWIMMER USED WITHIN THE Launcher.js file                                                                                              #
#                                                                                                                                                                   #
###################################################################################################################################################################*/

// ALL OTHER GLOBAL VARIABLES HAVE BEEN LEFT IN THE Launcher.js file

    // VARIABLES USED TO DETERMINE THE COORDINATES OF THE TORSO, WHICH IS ALSO USED TO DETERMINE THE PLACEMENT OF THE ARMS, LEGS AND NECK.
    var TorsoWidthA  = ( 2 * swimmerX * Math.sin( 1.25664 ) );      // EQUALS  0.95      72 Degrees     
    var TorsoHeightA = ( 2 * swimmerX * Math.cos( 1.25664 ) );      // EQUALS  0.31
    
    var TorsoWidthB  = ( 2 * swimmerX * Math.sin( 2.51327 ) );      // EQUALS  0.58      144 Degrees     
    var TorsoHeightB = ( 2 * swimmerX * Math.cos( 2.51327 ) );      // EQUALS -0.81

function createSwimmer( geometry, swimmerX, swimmerY, swimmerZ, material, eyeMaterial, swimsuitMaterial )
{
    // ADD TORSO
    var Torso = new createTorso( geometry, swimmerX, swimsuitMaterial, TorsoWidthA, TorsoHeightA, TorsoWidthB, TorsoHeightB );

    // MOVE TO NECK POSITION AND PLACE NECK JOINT
    var Neck = new createJoint( "Neck" );
    Torso.Neck = Neck;
    Neck.position.y += -TorsoHeightB;
    Neck.add( createAxes( axesLength ) );

    // MOVE TO HEAD POSITON AND PLACE HEAD
    var Head = new createHead( geometry, swimmerX, material );
    Head.position.y += swimmerX;

    var RightEye = new createEyes( swimmerX, eyeMaterial );
    RightEye.position.x += ( swimmerX / 4 );
    RightEye.position.y += ( swimmerX / 4 );
    RightEye.position.z += ( swimmerX / 4 );

    var LeftEye = new createEyes( swimmerX, eyeMaterial );
    LeftEye.position.x -= ( swimmerX / 4 );
    LeftEye.position.y += ( swimmerX / 4 );
    LeftEye.position.z += ( swimmerX / 4 );

    // ATTACH JOINTS TOGETHER AND TO THE TORSO
    Head.add( RightEye );
    Head.add( LeftEye );
    Neck.add( Head );
    Torso.add( Neck) ;      



    // ##### RIGHT ARM #####            
    // DRAW THE ARM
    RightArm = new createLimb( geometry, swimmerX, swimmerY, swimmerZ, material )
    Torso.RightArm = RightArm;

    // SHIFT RIGHT ARM INTO POSITION
    RightArm.position.x += TorsoWidthA;
    RightArm.position.y += TorsoHeightA;
    RightArm.rotation.z = ( 3.14 / 2 );     // ROTATE ARM TO FACE FORWARDS

    // ADD THE ENTIRE RIGHT ARM TO THE TORSO                      
    Torso.add( RightArm );



    // ##### LEFT ARM #####
    // DRAW THE ARM
    LeftArm = new createLimb( geometry, swimmerX, swimmerY, swimmerZ, material )
    Torso.LeftArm = LeftArm;

    // ROTATE THEN SHIFT LEFT ARM INTO POSITION
    LeftArm.rotation.z = 3.14;  // ROTATE 180 DEGREES TO LEFT SIDE OF THE BODY
    LeftArm.rotation.x = 3.14;  // ROTATE 180 DEGREES THE LEFT ARM SO THAT THE AXIS ARE IN THE CORRECT ORIENTATION
    LeftArm.position.x += -TorsoWidthA;
    LeftArm.position.y +=  TorsoHeightA;
    LeftArm.rotation.z = ( -3.14 / 2 );     // ROTATE ARM TO FACE FORWARDS

    // ADD THE ENTIRE LEFT ARM TO THE TORSO
    Torso.add( LeftArm );



    // ##### RIGHT LEG #####
    // DRAW THE LEG
    RightLeg = new createLimb( geometry, swimmerX, swimmerY, swimmerZ, material )
    Torso.RightLeg = RightLeg;

    // ROTATE THEN SHIFT RIGHT LEG INTO POSITION
    RightLeg.rotation.z = ( -3.14 / 2 );    // ROTATE 90 DEGREES
    RightLeg.position.x += TorsoWidthB;
    RightLeg.position.y += TorsoHeightB;

    // ADD THE ENTIRE RIGHT LEG TO THE TORSO
    Torso.add( RightLeg );



    // ##### LEFT LEG #####
    // DRAW THE LEG
    LeftLeg = new createLimb(geometry, swimmerX, swimmerY, swimmerZ, material)
    Torso.LeftLeg = LeftLeg;

    // ROTATE THEN SHIFT LEFT LEG INTO POSITION
    LeftLeg.rotation.z = ( -3.14 / 2 );    // ROTATE 90 DEGREES
    LeftLeg.position.x += -TorsoWidthB;
    LeftLeg.position.y +=  TorsoHeightB;

    // ADD THE ENTIRE LEFT LEG TO THE TORSO
    Torso.add( LeftLeg );

    return Torso;            
}   // END OF CreateSwimmer( material, eyeMaterial );



// CREATES AN OBJECT TO REPRESENT THE SWIMMERS Torso
function createTorso( geometry, swimmerX, swimsuitMaterial, TorsoWidthA, TorsoHeightA, TorsoWidthB, TorsoHeightB )                                        
{
/*                                  
                                           X 0, -TorsoHeightB  (Point 0)
                                         #   #
                                       #       #
(Point 4) -TorsoWidthA, TorsoHeightA X           X TorsoWidthA, TorsoHeightA   (Point 1)
                                      #    0    #
                                       #       #
   (Point 3) -TorsoWidthB, TorsoHeightB X # # X  TorsoWidthB, TorsoHeightB  (Point 2)
*/
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push(new THREE.Vector3(       0,      -TorsoHeightB, 0 ) );   // (Point 0)
    geometry.vertices.push(new THREE.Vector3(  TorsoWidthA,  TorsoHeightA, 0 ) );   // (Point 1)
    geometry.vertices.push(new THREE.Vector3(  TorsoWidthB,  TorsoHeightB, 0 ) );   // (Point 2)
    geometry.vertices.push(new THREE.Vector3( -TorsoWidthB,  TorsoHeightB, 0 ) );   // (Point 3)
    geometry.vertices.push(new THREE.Vector3( -TorsoWidthA,  TorsoHeightA, 0 ) );   // (Point 4)

    // DEPTH VERTICES
    geometry.vertices.push(new THREE.Vector3( 0, 0,  swimmerX ) );
    geometry.vertices.push(new THREE.Vector3( 0, 0, -swimmerX ) );

    // TOP LAYER OF FACES
    geometry.faces.push(new THREE.Face3( 0, 5, 1 ) );
    geometry.faces.push(new THREE.Face3( 1, 5, 2 ) );
    geometry.faces.push(new THREE.Face3( 2, 5, 3 ) );
    geometry.faces.push(new THREE.Face3( 3, 5, 4 ) );
    geometry.faces.push(new THREE.Face3( 4, 5, 0 ) );
    
    // BOTTOM LAYER OF FACES
    geometry.faces.push(new THREE.Face3( 1, 6, 0 ) );
    geometry.faces.push(new THREE.Face3( 2, 6, 1 ) );
    geometry.faces.push(new THREE.Face3( 3, 6, 2 ) );
    geometry.faces.push(new THREE.Face3( 4, 6, 3 ) );
    geometry.faces.push(new THREE.Face3( 0, 6, 4 ) );

    // TOP LAYER OF UV's
    var UVs = 
    [ 
        new THREE.Vector2(  0.5,    1  ), 
        new THREE.Vector2( 0.975, 0.65 ), 
        new THREE.Vector2(  0.79,  0.1 ), 
        new THREE.Vector2( 0.206,  0.1 ), 
        new THREE.Vector2(  0.03, 0.65 ),
        new THREE.Vector2(  0.5,   0.5 )
    ];

    geometry.faceVertexUvs [0] [ 0 ] = [ UVs[0], UVs[5], UVs[1] ];
    geometry.faceVertexUvs [0] [ 1 ] = [ UVs[1], UVs[5], UVs[2] ];
    geometry.faceVertexUvs [0] [ 2 ] = [ UVs[2], UVs[5], UVs[3] ];
    geometry.faceVertexUvs [0] [ 3 ] = [ UVs[3], UVs[5], UVs[4] ];
    geometry.faceVertexUvs [0] [ 4 ] = [ UVs[4], UVs[5], UVs[0] ];

    geometry.faceVertexUvs [0] [ 5 ] = [ UVs[1], UVs[5], UVs[0] ];
    geometry.faceVertexUvs [0] [ 6 ] = [ UVs[2], UVs[5], UVs[1] ];
    geometry.faceVertexUvs [0] [ 7 ] = [ UVs[3], UVs[5], UVs[2] ];
    geometry.faceVertexUvs [0] [ 8 ] = [ UVs[4], UVs[5], UVs[3] ];
    geometry.faceVertexUvs [0] [ 9 ] = [ UVs[0], UVs[5], UVs[4] ];

    geometry.computeFaceNormals();

    Torso = new THREE.Mesh( geometry, swimsuitMaterial ); 

    Torso.name = "Torso";
    Torso.add( createAxes( axesLength ) );

    BodyPartList.push( Torso );       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY (FOR MATERIAL CHANGES)

    return Torso;
}


// CREATE THE JOINT OBJECT AND ADD THE AXIS TO THE POINT
function createJoint( name )
{
    var Joint = new THREE.Object3D();
    Joint.add( createAxes( axesLength ) );

    return Joint;
}


// CREATES AN OBJECT TO REPRESENT THE SWIMMERS Head   
// PLEASE NOTE - I AM USING THE swimmerX VARIABLE IN THE PLACE OF A Y AND Z VALUE, THIS IS INTENTIONAL TO MAINTAIN A CONSISTENT SHAPE FOR THE HEAD IN COMPARISON TO THE BODY
function createHead( geometry, swimmerX, material )            
{
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push(new THREE.Vector3(     0,     swimmerX, 0 ) );
    geometry.vertices.push(new THREE.Vector3(  swimmerX,    0,     0 ) );
    geometry.vertices.push(new THREE.Vector3(     0,    -swimmerX, 0 ) );
    geometry.vertices.push(new THREE.Vector3( -swimmerX,    0,     0 ) );

    // DEPTH VERTICES
    geometry.vertices.push(new THREE.Vector3( 0, 0,  swimmerX ) );
    geometry.vertices.push(new THREE.Vector3( 0, 0, -swimmerX ) );


    // FRONT TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3( 0, 3, 4 ) );
    geometry.faces.push(new THREE.Face3( 0, 4, 1 ) );

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3( 2, 4, 3 ) );
    geometry.faces.push(new THREE.Face3( 2, 1, 4 ) );
   
    // REAR TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3( 0, 5, 3 ) );
    geometry.faces.push(new THREE.Face3( 0, 1, 5 ) );

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3( 2, 3, 5 ) );
    geometry.faces.push(new THREE.Face3( 2, 5, 1 ) );

    geometry.computeFaceNormals();

    Head = new THREE.Mesh( geometry, material ); 

    Head.name = "Head";
    Head.add( createAxes( axesLength ) );

    BodyPartList.push( Head );       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY (FOR MATERIAL CHANGES)

    return Head;
}


// CREATES AND RETURNS A Spherical SHAPE TO REPRESENT THE EYES OF THE SWIMMER
function createEyes( swimmerX, eyeMaterial )                     
{ 
    geometry = new THREE.SphereGeometry( swimmerX / 4 );

    geometry.computeFaceNormals();

    eye = new THREE.Mesh( geometry, eyeMaterial ); 

    eye.name = "eye";
    return eye;
}


// CREATES ALL THE OBJECTS NEEDED TO CREATE AN ARM OR A LEG 
function createLimb( geometry, swimmerX, swimmerY, swimmerZ, material )
{
    var Limb = new THREE.Object3D();

    // CREATE UpperJoint - USED FOR Shoulder OR Hip
    var UpperJoint = new createJoint( UpperJoint );
    UpperJoint.name = "UpperJoint";

    // CREATE UpperLimb AND MOVE TO POSITION
    var UpperLimb = new createOctahedron( geometry, swimmerX, swimmerY, swimmerZ, material );
    UpperLimb.name = "UpperLimb"; 
    UpperLimb.position.x += swimmerX;

    // CREATE CentralJoint AND MOVE TO POSITION - USED FOR Elbow OR Knee
    var CentralJoint = new createJoint( CentralJoint );
    CentralJoint.name = "CentralJoint";
    CentralJoint.position.x += swimmerX;

    // CREATE LowerLimb AND MOVE TO POSITION
    var LowerLimb = new createOctahedron( geometry, swimmerX, swimmerY, swimmerZ, material );
    LowerLimb.name = "LowerLimb";
    LowerLimb.position.x += swimmerX; 

    // CREATE LowerJoint AND MOVE TO POSITION - USED FOR Wrist OR Ankle
    var LowerJoint = new createJoint( LowerJoint );
    LowerJoint.name = "LowerJoint";
    LowerJoint.position.x += swimmerX;

    // CREATE HAND AND MOVE TO POSITION
    var Hand = new createDecahedron( geometry, swimmerX, swimmerY, swimmerZ, material );  
    Hand.name = "Hand";

    // CONNECT THE HEIRACHY OF THE LIMB BEFORE RETURNING IT TO THE CALLING FUNCTION
    Limb.add( UpperJoint );
    UpperJoint.add( UpperLimb );
    UpperLimb.add( CentralJoint );
    CentralJoint.add( LowerLimb );
    LowerLimb.add( LowerJoint );
    LowerJoint.add( Hand );

    Limb.UpperJoint = UpperJoint;
    Limb.UpperLimb = UpperLimb;
    Limb.CentralJoint = CentralJoint;
    Limb.LowerLimb = LowerLimb;
    Limb.LowerJoint = LowerJoint;
    Limb.Hand = Hand;

return Limb;
}


// CREATES AND RETURNS AN Octahedron OBJECT TO REPRESENT THE LIMBS OF THE SWIMMER
function createOctahedron( geometry, swimmerX, swimmerY, swimmerZ, material )                 
{
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push( new THREE.Vector3(    0 ,    swimmerY, 0 ) );
    geometry.vertices.push( new THREE.Vector3(  swimmerX,    0,    0 ) );
    geometry.vertices.push( new THREE.Vector3(    0,    -swimmerY, 0 ) );
    geometry.vertices.push( new THREE.Vector3( -swimmerX,    0,    0 ) );

    // DEPTH VERTICES
    geometry.vertices.push( new THREE.Vector3( 0, 0,  swimmerZ ) );
    geometry.vertices.push( new THREE.Vector3( 0, 0, -swimmerZ ) );


    // FRONT TOP HALF FRONT FACES
    geometry.faces.push( new THREE.Face3( 0, 3, 4 ) );
    geometry.faces.push( new THREE.Face3( 0, 4, 1 ) );

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push( new THREE.Face3( 2, 4, 3 ) );
    geometry.faces.push( new THREE.Face3( 2, 1, 4 ) );
   
    // REAR TOP HALF FRONT FACES
    geometry.faces.push( new THREE.Face3( 0, 5, 3 ) );
    geometry.faces.push( new THREE.Face3( 0, 1, 5 ) );

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push( new THREE.Face3( 2, 3, 5 ) );
    geometry.faces.push( new THREE.Face3( 2, 5, 1 ) );

    geometry.computeFaceNormals();

    Octahedron = new THREE.Mesh( geometry, material ); 

    Octahedron.name = "Octahedron";
    Octahedron.add( createAxes( axesLength ) );

    BodyPartList.push( Octahedron );       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY

    return Octahedron;
}


// CREATES AND RETURNS A Decahedron OBJECT TO REPRESENT THE HANDS AND FEET OF THE SWIMMER
function createDecahedron( geometry, swimmerX, swimmerY, swimmerZ, material )                     
{
    geometry = new THREE.Geometry();

    // APEX OF SHAPE
    geometry.vertices.push( new THREE.Vector3(0, 0, 0));

    // BASE OF SHAPE
    geometry.vertices.push( new THREE.Vector3( swimmerX,  swimmerY,  swimmerZ ) );
    geometry.vertices.push( new THREE.Vector3( swimmerX, -swimmerY,  swimmerZ ) );
    geometry.vertices.push( new THREE.Vector3( swimmerX, -swimmerY, -swimmerZ ) );
    geometry.vertices.push( new THREE.Vector3( swimmerX,  swimmerY, -swimmerZ ) );


    // UPWARD FACING SIDE OF SHAPE
    geometry.faces.push( new THREE.Face3( 1, 4, 0 ) );

    // FORWARD FACING SIDE OF SHAPE
    geometry.faces.push( new THREE.Face3( 2, 1, 0 ) );

    // DOWNWARD FACING SIDE OF SHAPE
    geometry.faces.push( new THREE.Face3( 3, 2, 0 ) );
   
    // REAR FACING SIDE OF SHAPE
    geometry.faces.push( new THREE.Face3( 4, 3, 0 ) );

    // SQUARE BASE OF SHAPE
    geometry.faces.push( new THREE.Face3( 3, 4, 1 ) );
    geometry.faces.push( new THREE.Face3( 1, 2, 3 ) );

    geometry.computeFaceNormals();

    Decahedron = new THREE.Mesh( geometry, material ); 

    Decahedron.name = "Decahedron";

    BodyPartList.push( Decahedron );       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY

    return Decahedron;
}