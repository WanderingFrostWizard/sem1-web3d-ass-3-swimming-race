/*###################################################################################################################################################################
#       FILENAME - CreatePool.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                           #
#                                                                                                                                                                   # 
#       THE SWIMMING RACE USING Javascript and Three.js                                                                                                             #
#       WRITTEN BY CAMERON WATT s3589163                                                                                                                            #
#       COMPLETED 19 MAY 2016                                                                                                                                       #
#                                                                                                                                                                   #
#       THIS FILE CREATES THE POOL USED WITHIN THE SwimmingRace.js FILE                                                                                             #
#       ### NOTE ###  BOTTOM OF THE POOL (outwards side, looking in) IS intentionally left blank, to show the swimmers better from underneath                       #
#                                                                                                                                                                   #
###################################################################################################################################################################*/

function createPool( geometry, poolX, poolY, poolZ, poolMaterial, poolTexture )
{
    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3( -poolX, -poolY, poolZ) );
    geometry.vertices.push(new THREE.Vector3(  poolX, -poolY, poolZ) );
    geometry.vertices.push(new THREE.Vector3(  poolX,  poolY, poolZ) );
    geometry.vertices.push(new THREE.Vector3( -poolX,  poolY, poolZ) );

    // Rear Face Declaration
    geometry.vertices.push(new THREE.Vector3(-poolX, -poolY, -poolZ) );
    geometry.vertices.push(new THREE.Vector3( poolX, -poolY, -poolZ) );
    geometry.vertices.push(new THREE.Vector3( poolX,  poolY, -poolZ) );
    geometry.vertices.push(new THREE.Vector3(-poolX,  poolY, -poolZ) );

    // Front Face Display 
            // Inner Face
    geometry.faces.push(new THREE.Face3( 0, 1, 2 ) );
    geometry.faces.push(new THREE.Face3( 2, 3, 0 ) ); 
            // Outer Face
    geometry.faces.push(new THREE.Face3( 3, 2, 1 ) );
    geometry.faces.push(new THREE.Face3( 1, 0, 3 ) ); 


    // Right Hand Side Face Display
            // Inner Face
    geometry.faces.push(new THREE.Face3( 5, 1, 2 ) );
    geometry.faces.push(new THREE.Face3( 2, 6, 5 ) );
            // Outer Face
    geometry.faces.push(new THREE.Face3( 6, 2, 1 ) );
    geometry.faces.push(new THREE.Face3( 1, 5, 6 ) );


    // Rear Face Display   
            // Inner Face              
    geometry.faces.push(new THREE.Face3( 4, 5, 6 ) );
    geometry.faces.push(new THREE.Face3( 6, 7, 4 ) );
            // Outer Face
    geometry.faces.push(new THREE.Face3( 7, 6, 5 ) );
    geometry.faces.push(new THREE.Face3( 5, 4, 7 ) ); 


    // Left Hand Side Face Display
            // Inner Face  
    geometry.faces.push(new THREE.Face3( 7, 3, 0 ) );
    geometry.faces.push(new THREE.Face3( 0, 4, 7 ) );
            // Outer Face
    geometry.faces.push(new THREE.Face3( 3, 7, 4 ) );
    geometry.faces.push(new THREE.Face3( 4, 0, 3 ) );     


    // Bottom Face Display             
    geometry.faces.push(new THREE.Face3( 1, 5, 4 ) );
    geometry.faces.push(new THREE.Face3( 4, 0, 1 ) );

    var UVs = 
    [ 
        new THREE.Vector2( 0, 0 ), 
        new THREE.Vector2( 1, 0 ), 
        new THREE.Vector2( 1, 1 ), 
        new THREE.Vector2( 0, 1 )
    ];

    // UV coordinates for texture mapping
    // Change UVs for EACH face, each of which contains 2 triangles (The left and right sides have inside and outside textures)
    for (var i = 0; i < 9; i++) 
    {
        geometry.faceVertexUvs [0] [ i * 2 ] = [ UVs[0], UVs[1], UVs[2] ];
        geometry.faceVertexUvs [0] [ i * 2 + 1 ] = [ UVs[2], UVs[3], UVs[0] ];
    }

    geometry.computeFaceNormals();

    var pool = new THREE.Mesh( geometry, poolMaterial );

    // TEXTURE REPEATION STRUCTURE
    poolTexture.repeat.set( 3, 0.5 );
    poolTexture.wrapS = THREE.RepeatWrapping;
    poolTexture.wrapT = THREE.RepeatWrapping;
    return pool;
}

// CREATES THE WATER MATERIAL FOR THE POOL ( USES waterHeight INSTEAD OF poolY )
function createWater ( geometry, poolX, waterHeight, poolZ, waterMaterial )
{
    geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3( -poolX, waterHeight,  poolZ) );
    geometry.vertices.push(new THREE.Vector3(  poolX, waterHeight,  poolZ) );
    geometry.vertices.push(new THREE.Vector3(  poolX, waterHeight, -poolZ) );
    geometry.vertices.push(new THREE.Vector3( -poolX, waterHeight, -poolZ) );

    geometry.faces.push(new THREE.Face3( 0, 1, 2 ) );   
    geometry.faces.push(new THREE.Face3( 2, 3, 0 ) ); 

    geometry.computeFaceNormals();

    var water = new THREE.Mesh( geometry, waterMaterial );
    return water;
}


function createRopeDivider(poolX, waterHeight, poolZ, geometry, ropeMaterial)
{
    // THREE.CylinderGeometry(radiusTop, radiusBottom, height, radiusSegments, heightSegments, openEnded, thetaStart, thetaLength)
    geometry = new THREE.CylinderGeometry( 0.05, 0.05, ( poolZ * 2 ), 8 );
    rope = new THREE.Mesh( geometry, ropeMaterial );
    rope.rotation.x = ( 3.14 / 2 );  // ROTATE TO BE ALONG THE Z AXIS
    rope.position.y = waterHeight;   // MOVE POOL ROPE UP TO THE WATER HEIGHT
    rope.position.x -= poolX         // START FROM THE LEFT HAND EDGE OF THE POOL

    return rope;
}